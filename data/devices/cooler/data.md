---
name: "BQ Aquaris M10 HD"
deviceType: "tablet"
description: "The first Ubuntu tablet and the first to offer desktop-mobile convergence, the rear camera shoots surprisingly good stills, particularly in HDR mode. A quad-core processor powers the M10, 64-bit MediaTek MT8163 SoC clocked at 1.3GHz in the HD model and 1.5GHz in the FHD model. The GPU is a Mali-T720 MP2, clocked up to 520MHz (HD) and 600MHz (FHD). Both models have 2GB of RAM and 16GB of internal storage, which can be expanded by up to 64GB by plugging a card into the external MicroSD slot."
subforum: "75/bq-m10-hd"

deviceInfo:
  - id: "cpu"
    value: "Quad-Core Cortex-A53 1.3Ghz"
  - id: "chipset"
    value: "MediaTek MT8163B"
  - id: "gpu"
    value: "Mali-T720MP2"
  - id: "rom"
    value: "16GB"
  - id: "ram"
    value: "2GB"
  - id: "android"
    value: "Android 5.1"
  - id: "battery"
    value: "7280 mAh"
  - id: "display"
    value: "1280x800 pixels, 10.1 in"
  - id: "rearCamera"
    value: "5MP"
  - id: "frontCamera"
    value: "2MP"

contributors:
  - name: "BQ"
    role: "Phone maker"
    expired: true
---
